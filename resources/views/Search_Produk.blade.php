<html>
<head>
	<title>Search Product</title>
	 
	<style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}

		table {
			width: 50%;
			border-collapse: collapse;
			border: 1px solid black;
		}
		
		th {
			height: 50px;
		}

		.container {
			position:static;
		}

		.center {
			position: absolute;
 			 top: 8%;
  			width: 100%;
		}
	</style>
	
</head>
<body>
	<h2>Search Produk</h2>
	<div class="center">
		<div>
			<table border="1">
				<tr>
					<th>Nama Barang</th>
					<th>Harga</th>
					<th>Kategori</th>
				</tr>
				@foreach($produk as $p)
				<tr>
					<td>{{ $p->NAMA_PRODUK }}</td>
					<td>{{ $p->HARGA_PRODUK }}</td>
					<td>{{ $p->KATEGORI }}</td>
				</tr>
				@endforeach
			</table>
		</div>

		<br>

		<div>
			<form action="/produk/search" method="GET">
			<input type="text" name="search" placeholder="Cari Barang .." value="{{ old('search') }}">
			<input type="submit" value="CARI">
			</form>
		</div>

	</div>
</body>
</html>

